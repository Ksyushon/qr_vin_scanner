package org.itstep.degtyar.qrscanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText winScanner;
    EditText qrScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        winScanner=(EditText)findViewById(R.id.editTextWin);
        qrScanner=(EditText)findViewById(R.id.editTextQR);

        Button vinButton=(Button)findViewById(R.id.buttonVin);
        Button qrButton=(Button)findViewById(R.id.buttonQR);

        vinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scanVin();
            }
        });


        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scanQR();
            }
        });
    }


    public void scanVin() {

        Log.d("tag", "scanVin()");

        Intent intent = new Intent(this, ZbarScanCodeActivity.class);
        intent.putExtra(Globals.TYPE_OF_SCANNER, Globals.REQUEST_CODE_VIN_SCANNER);
        startActivityForResult(intent, Globals.REQUEST_CODE_VIN_SCANNER);

    }



    public void scanQR() {

        Log.d("tag", "scanQR()");
        Intent intent = new Intent(this, ZbarScanCodeActivity.class);
        intent.putExtra(Globals.TYPE_OF_SCANNER, Globals.REQUEST_CODE_QR_SCANNER);
        startActivityForResult(intent, Globals.REQUEST_CODE_QR_SCANNER);

    }



    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("tag", "onActivityResult");

        Log.d("tag", "requestCode ­ " + requestCode);

        Log.d("tag", "resultCode ­ " + resultCode);

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Globals.REQUEST_CODE_VIN_SCANNER) {

                String tempBar = data.getStringExtra(Globals.KEY_SCAN_RESULT);

                if (tempBar != null) {

                    if (tempBar.length() > 17) {

                        tempBar = tempBar.substring(tempBar.length()-17,tempBar.length());
                    }

                    winScanner.setText(tempBar);
                }

            }


                if (requestCode == Globals.REQUEST_CODE_QR_SCANNER) {

                    String tempBar = data.getStringExtra(Globals.KEY_SCAN_RESULT);

                    if (tempBar != null) {

                        if (tempBar.length() > 17) {

                            tempBar = tempBar.substring(tempBar.length()-17, tempBar.length());
                        }

                        Log.d("tag", "onActivityResult ­ editDealer.setText(tempBar);");

                        qrScanner.setText(tempBar);

                    }

                }


        } else {

            Log.d("tag", "resultCode != Activity.RESULT_OK");

        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
